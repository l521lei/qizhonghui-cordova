package it.maichong.plugins;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import cz.msebera.android.httpclient.Header;

public class Network extends CordovaPlugin {

    final static String TAG = "Network Plugin";

    AsyncHttpClient client = new AsyncHttpClient();


    @Override
    public boolean execute(String action, JSONArray args,
                           final CallbackContext callbackContext) throws JSONException {
        if (action.equals("post")) {
            return post(args, callbackContext);
        }

        if (action.equals("connect")) {
            callbackContext.success();
        }
        return true;
    }


    public boolean post(JSONArray args,
                        final CallbackContext callbackContext) throws JSONException {


        final String url = args.getString(0);

        JSONObject data = args.getJSONObject(1);

        //Log.i(TAG, data);

        RequestParams params = new RequestParams();
        Iterator<?> it = data.keys();
        while (it.hasNext()) {//遍历JSONObject
            String key = (String) it.next().toString();
            String value = data.getString(key);
            params.put(key, value);
        }

        client.post(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"

                //Log.e(TAG, "onSuccess");
                try {
                    String result = new String(response);
                    //Log.i(TAG, result);
                    JSONObject obj = new JSONObject(result);
                    callbackContext.success(obj);
                } catch (Exception e) {
                    callbackContext.error("-1");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {

                Log.e(TAG, "onFailure");
                callbackContext.error("-1");
            }

        });

        return true;
    }
}
