
#import "WXPay.h"


#import <Cordova/CDVJSON.h>

#import "WXApi.h"

#import "WXApiObject.h"

@implementation WXPay

- (void)pay:(CDVInvokedUrlCommand*)command
{
    NSDictionary* req = [command argumentAtIndex:0];
    //NSLog(@"%@ ",req);
    
    NSOperationQueue *operationQueue = [[NSOperationQueue alloc]init];
    [operationQueue addOperationWithBlock:^{
        
        //微信支付
        [WXApi registerApp:@"wx0f2e783b96299ff4" withDescription:@"Maichong"];
        
        PayReq *request = [[PayReq alloc] init];
        
        request.partnerId = [req objectForKey:@"partnerId"];
        request.prepayId= [req objectForKey:@"prepayId"];
        request.package = [req objectForKey:@"package"];
        request.nonceStr= [req objectForKey:@"nonceStr"];
        NSNumber *timeStamp= [req objectForKey:@"timeStamp"];
        request.timeStamp=[timeStamp unsignedLongValue];
        request.sign= [req objectForKey:@"sign"];
        
        
        [WXApi sendReq:request];
        
    }];
    
    
}

@end