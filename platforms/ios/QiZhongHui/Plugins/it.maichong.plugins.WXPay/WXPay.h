/**
 liang@maichong.it
 */


#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>



@interface WXPay : CDVPlugin{}

- (void)pay:(CDVInvokedUrlCommand*)command;

@end

