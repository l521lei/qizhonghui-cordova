/**
 liang@maichong.it
 */


#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

@interface Share : CDVPlugin{}


- (void)share:(CDVInvokedUrlCommand*)command;

@end

