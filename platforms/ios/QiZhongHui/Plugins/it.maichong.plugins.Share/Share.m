

#import "Share.h"

#import "UMSocial.h"
#import "UMSocialWechatHandler.h"
#import "UMSocialSinaHandler.h"


@implementation Share


// 友盟appKey
#define UM_APPKEY @"56ac6ab267e58ec556002021"

// 微信App ID 注意，此ID为微信公众平台APP ID
#define WX_APPID @"wxfb5710e657c61c89"

// 微信App 秘钥
#define WX_SECRET @"57dc5a92939e1758a324bc860a9c74bb"

// 网站地址
#define WEBSITE @"http://www.qizhonghui.cn/m"



- (void)share:(CDVInvokedUrlCommand*)command
{
    NSLog(@"Share");
    
    NSString* title = [command argumentAtIndex:0];
    NSString* url = [command argumentAtIndex:1];
    NSString* pic = [command argumentAtIndex:2];
    //NSString* content = [command argumentAtIndex:3];
    
    
    
    //友盟分享
    [UMSocialData setAppKey:UM_APPKEY];
    [UMSocialWechatHandler setWXAppId:WX_APPID appSecret:WX_SECRET url:WEBSITE];
    [UMSocialSinaHandler openSSOWithRedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    
    
    /*
     UIImage *img=nil;
     
     if([pic length]){
     img=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:pic]]];;
     }
     */
    
    [[UMSocialData defaultData].urlResource setResourceType:UMSocialUrlResourceTypeImage url:pic];
    
    [UMSocialData defaultData].extConfig.wechatSessionData.url = url;
    
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = url;
    
    [UMSocialData defaultData].extConfig.wechatSessionData.title =title;
    
    [UMSocialData defaultData].extConfig.wechatTimelineData.title =title;
    
    [UMSocialSnsService presentSnsIconSheetView:self.viewController
                                         appKey:UM_APPKEY // 友盟appKey
                                      shareText:title
                                     shareImage:[UIImage imageNamed:@"icon"]
                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToWechatSession,UMShareToWechatTimeline,UMShareToEmail,nil]
                                       delegate:nil];
  /*
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    */
    
    
    
}


@end