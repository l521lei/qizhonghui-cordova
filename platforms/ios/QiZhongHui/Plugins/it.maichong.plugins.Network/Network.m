
#import "Network.h"

#import "AFHTTPRequestOperationManager.h"

//#import <Cordova/CDVJSON.h>


@implementation Network

- (void)post:(CDVInvokedUrlCommand*)command
{
    
    NSString* url = [command argumentAtIndex:0];
    //NSLog(@"%@",command);
    NSDictionary* data = [command argumentAtIndex:1];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //NSLog(@"Net request: %@ %@",url,data);
    [manager POST:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"JSON: %@", responseObject);
        //NSLog(@"Net success: %@",url);
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:responseObject];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@  URL: %@  DATA: %@ RESULT: %@", error,url,data,operation.responseString);
        
        
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"-1"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}


- (void)connect:(CDVInvokedUrlCommand*)command
{
    //NSLog(@"Net connect");
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}
@end
