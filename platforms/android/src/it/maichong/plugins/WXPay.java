package it.maichong.plugins;


import android.util.Log;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import com.tencent.mm.sdk.openapi.*;
import com.tencent.mm.sdk.modelpay.PayReq;


public class WXPay extends CordovaPlugin {
	public static final String TAG = "WXPay Plugin";

	public static final String appId="wxfb5710e657c61c89";

	private static WXPay instance;

	public static WXPay getInstance() {
		return instance;
	}

	private CallbackContext callbackContext;


	@Override
	public boolean execute(String action, JSONArray args,
						   final CallbackContext callbackContext) throws JSONException {
		instance=this;
		if(action.equals("pay")){
			return pay(args,callbackContext);
		}
		return true;
	}

	public void onResult(int code){
		Log.e(TAG,"CODE:"+code);
		callbackContext.success(String.valueOf(code));
	};

	private boolean pay(JSONArray args,
						final CallbackContext callbackContext) throws JSONException{

		this.callbackContext=callbackContext;

		final JSONObject req=args.getJSONObject(0);
		Log.i(TAG, req.toString());

		new Thread() {
			public void run() {

				try{

					IWXAPI wxapi = WXAPIFactory.createWXAPI(WXPay.this.cordova.getActivity(), null);
					wxapi.registerApp(WXPay.appId);

					PayReq request = new PayReq();
					request.appId = WXPay.appId;
					request.partnerId = req.getString("partnerId");
					request.prepayId= req.getString("prepayId");
					request.packageValue = req.getString("package");
					request.nonceStr= req.getString("nonceStr");
					request.timeStamp= String.valueOf(req.getInt("timeStamp"));
					request.sign= req.getString("sign");

					wxapi.sendReq(request);
				}catch(JSONException e){
					WXPay.this.callbackContext.error("-3");
				}
			}
		}.start();



		return true;
	}
}
