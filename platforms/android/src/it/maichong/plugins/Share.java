package it.maichong.plugins;

import android.os.Handler;
import android.util.Log;
import android.app.Activity;
import android.content.Intent;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.listener.SocializeListeners;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.sso.EmailHandler;
import com.umeng.socialize.sso.SmsHandler;
import com.umeng.socialize.sso.SinaSsoHandler;
import com.umeng.socialize.sso.UMSsoHandler;


import com.umeng.socialize.weixin.controller.UMWXHandler;
import com.umeng.socialize.weixin.media.WeiXinShareContent;
import com.umeng.socialize.weixin.media.CircleShareContent;


public class Share extends CordovaPlugin {

	public static final String TAG = "Share Plugin";

	private UMSocialService mController;

	private static Share instance;

	public static Share getInstance() {
		return instance;
	}

	@Override
	public boolean execute(String action, JSONArray args,
						   final CallbackContext callbackContext) throws JSONException {


		//友盟分享
		mController = UMServiceFactory.getUMSocialService("com.umeng.share");


		instance = this;


		// 设置分享内容
		final String title = args.getString(0);
		final String url = args.getString(1);
		final String pic = args.getString(2);
		final String content = args.getString(3);

		Log.d(TAG, title + " " + url + " " + pic + " " + content);
		Log.d(TAG, args.toString());

		final Activity activity = this.cordova.getActivity();

		//ZhiAi.getInstance().share=this;

		//final ZhiAi activity=ZhiAi.getInstance();

		//activity.share=this;

		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {


				//微信开放平台 》 移动应用信息
				String appId = "wxfb5710e657c61c89";
				String appSecret = "57dc5a92939e1758a324bc860a9c74bb";
				// 添加微信平台
				UMWXHandler wxHandler = new UMWXHandler(activity, appId, appSecret);
				wxHandler.addToSocialSDK();
				// 添加微信朋友圈
				UMWXHandler wxCircleHandler = new UMWXHandler(activity, appId, appSecret);
				wxCircleHandler.setToCircle(true);
				wxCircleHandler.addToSocialSDK();


				mController.setShareContent(content);
				// 设置分享图片, 参数2为图片的url地址

				EmailHandler emailHandler = new EmailHandler();
				emailHandler.addToSocialSDK();

				//短信
				SmsHandler smsHandler = new SmsHandler();
				smsHandler.addToSocialSDK();

				//mController.getConfig().setSsoHandler(new SinaSsoHandler());
				//mController.getConfig().removePlatform(SHARE_MEDIA.TENCENT);
				mController.getConfig().setPlatforms(
						SHARE_MEDIA.WEIXIN,
						SHARE_MEDIA.WEIXIN_CIRCLE,
						//SHARE_MEDIA.QZONE,
						//SHARE_MEDIA.QQ,
						//SHARE_MEDIA.TENCENT,
						SHARE_MEDIA.SINA,
						//SHARE_MEDIA.DOUBAN,
						SHARE_MEDIA.EMAIL
				);

				//设置微信好友分享内容
				WeiXinShareContent weixinContent = new WeiXinShareContent();
				//设置分享文字
				weixinContent.setShareContent(content);
				//设置title
				weixinContent.setTitle(title);
				//设置分享内容跳转URL
				weixinContent.setTargetUrl(url);

				//设置微信朋友圈分享内容
				CircleShareContent circleContent = new CircleShareContent();
				circleContent.setShareContent(content);
				//设置朋友圈title
				circleContent.setTitle(title);
				circleContent.setTargetUrl(url);


				//设置分享图片
				if (!pic.isEmpty()) {
					UMImage img = new UMImage(activity, pic);
					weixinContent.setShareImage(img);
					circleContent.setShareImage(img);
					mController.setShareMedia(img);
				}
				mController.setShareMedia(weixinContent);
				mController.setShareMedia(circleContent);

				mController.openShare(activity, false);
			}
		});
		SocializeListeners.SnsPostListener mSnsPostListener = new SocializeListeners.SnsPostListener() {

			@Override
			public void onStart() {

			}

			@Override
			public void onComplete(SHARE_MEDIA platform, int stCode,
								   SocializeEntity entity) {
				if (stCode == 200) {
					Log.d(TAG, "分享成功");
					if (platform == SHARE_MEDIA.WEIXIN || platform == SHARE_MEDIA.WEIXIN_CIRCLE) {
						callbackContext.success();
					}
				} else {
					Log.d(TAG, "分享失败");
				}
			}
		};
		mController.registerListener(mSnsPostListener);
		return true;
	}


	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (mController == null) {
			return;
		}
		//使用SSO授权必须添加如下代码
		UMSsoHandler ssoHandler = mController.getConfig().getSsoHandler(requestCode);
		if (ssoHandler != null) {
			ssoHandler.authorizeCallBack(requestCode, resultCode, data);
		}
	}
}
