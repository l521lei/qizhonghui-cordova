package it.maichong.qizhonghui.wxapi;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import it.maichong.plugins.WXPay;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {

	private static final String TAG = "WXPayEntryActivity";

	private IWXAPI api;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		//Log.d(TAG, "onCreate");


		api = WXAPIFactory.createWXAPI(this, WXPay.appId);
		api.handleIntent(getIntent(), this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		//Log.d(TAG, "onNewIntent");
		api.handleIntent(intent, this);
	}

	@Override
	public void onReq(BaseReq req) {
		//Log.d(TAG, "onReq");
	}

	@Override
	public void onResp(BaseResp resp) {
		Log.d(TAG, "onResp");
		Log.i(TAG,resp.toString());

		WXPay.getInstance().onResult(resp.errCode);

		finish();

	}

}
